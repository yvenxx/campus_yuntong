from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.template import loader


def register(request):
    template = loader.get_template('account/register.html')
    context = {}
    return HttpResponse(template.render(context,request))


def login(request):
    if request.COOKIES.get("is_login"):
        return redirect('/forum/templates/forum/Confession.html')
    template = loader.get_template('account/login.html')
    context = {}
    return HttpResponse(template.render(context,request))


def logout(request):
    rep = redirect('/account/login/')
    rep.delete_cookie("is_login")
    rep.delete_cookie('user_id')
    rep.delete_cookie('user_name')
    rep.delete_cookie('user_img')
    return rep  # 点击注销后执行,删除cookie,不再保存用户状态，并弹到登录页面