from datetime import date

from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.template import loader
from django.views import View

from backstage.models import BackUser, DayNumber
from backstage.utils import visit_count
from backstage.utils.visit_count import five_visit
from forum.models import PostInfo, UserInfo


def index(request):
    template = loader.get_template('backstage/back_login.html')
    context = {}
    return HttpResponse(template.render(context))


class backstage_manage(View):

    def get(self,request):
        if not request.COOKIES.get("is_login_back"):
            return redirect('/backstage/')

        user = request.COOKIES.get("back_user")


        # 审核
        unreviewed = PostInfo.objects.filter(reviewed=0)
        past = PostInfo.objects.filter(reviewed=1)
        notpast = PostInfo.objects.filter(reviewed=2)

        unreviewed_num = PostInfo.objects.filter(reviewed=0).count()
        past = PostInfo.objects.filter(reviewed=1).count()
        notpast = PostInfo.objects.filter(reviewed=2).count()

        user_num = UserInfo.objects.all().count()

        fiveday_visit_num = five_visit(request)


        post_num = PostInfo.objects.filter(date=date.today()).count()

        visit_now = visit_count.visit_count(request)

        template = loader.get_template('backstage/backstage_manage.html')
        context = {
            'back_user':user,
            'visit':visit_now,
            'unreviewed_num':unreviewed_num,
            'past':past,
            'nopast':notpast,
            'user_num':user_num,
            'post_num':post_num,
            'five_day_visit':fiveday_visit_num,
        }
        return HttpResponse(template.render(context))


def logout(request):
    rep = redirect('/backstage/')
    rep.delete_cookie("is_login_back")
    rep.delete_cookie("back_user")
    return rep  # 点击注销后执行,删除cookie,不再保存用户状态，并弹到登录页面


def usermanage(request,page):
    if not request.COOKIES.get("is_login_back"):
        return redirect('/backstage/')

    user_start = int(page) * 10 - 10
    user_end = int(page) * 10

    page_end_num = UserInfo.objects.all().count() / 10

    user = UserInfo.objects.all()[user_start:user_end]

    user_num = UserInfo.objects.all().count()
    template = loader.get_template('backstage/usermanage.html')
    back_user = request.COOKIES.get("back_user")
    context = {
        'message':user,
        'back_user':back_user,
        'message_num':user_num,
        'page':page,
        'page_end_num':page_end_num,
    }
    return HttpResponse(template.render(context))


# all post
def postmanage(request,page):
    if not request.COOKIES.get("is_login_back"):
        return redirect('/backstage/')

    post_start = int(page) * 5 - 5
    post_end = int(page) * 5

    page_end_num = PostInfo.objects.all().count() / 5

    post = PostInfo.objects.all().order_by('-date')[post_start:post_end]
    post_num = PostInfo.objects.all().count()
    back_user = request.COOKIES.get("back_user")

    context = {
        'message': post,
        'back_user': back_user,
        'message_num': post_num,
        'page':page,
        'page_end_num':page_end_num
    }

    template = loader.get_template('backstage/postmanage.html')

    return HttpResponse(template.render(context))


def unreviewed_manage(request):
    if not request.COOKIES.get("is_login_back"):
        return redirect('/backstage/')
    post = PostInfo.objects.filter(reviewed="unreviewed").order_by('-date')
    back_user = request.COOKIES.get("back_user")
    context = {
        'message': post,
        'back_user': back_user,
    }
    template = loader.get_template('backstage/postmanage.html')

    return HttpResponse(template.render(context))


def past_manage(request):
    if not request.COOKIES.get("is_login_back"):
        return redirect('/backstage/')
    post = PostInfo.objects.filter(reviewed="past").order_by('-date')
    back_user = request.COOKIES.get("back_user")

    context = {
        'message': post,
        'back_user': back_user,
    }

    template = loader.get_template('backstage/postmanage.html')

    return HttpResponse(template.render(context))


def notpast_manage(request):
    if not request.COOKIES.get("is_login_back"):
        return redirect('/backstage/')
    post = PostInfo.objects.filter(reviewed="notpast").order_by('-date')
    back_user = request.COOKIES.get("back_user")

    context = {
        'message': post,
        'back_user': "back_user",
    }

    template = loader.get_template('backstage/postmanage.html')

    return HttpResponse(template.render(context))


def delete_post(request,postid):
    post = PostInfo.objects.get(post_id=postid)
    post.delete()
    return redirect('/backstage/postmanage/1/')

# verify
def past(request,postid):
    post = PostInfo.objects.get(post_id=postid)
    post.reviewed = "past"
    post.save()
    return redirect('/backstage/postmanage/1/')


def notPast(request,postid):
    post = PostInfo.objects.get(post_id=postid)
    post.reviewed = "notpast"
    post.save()
    return redirect('/backstage/postmanage/1/')