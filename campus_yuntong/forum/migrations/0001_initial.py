# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2020-06-24 04:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PostInfo',
            fields=[
                ('post_id', models.AutoField(max_length=11, primary_key=True, serialize=False, verbose_name='POSTID')),
                ('post_title', models.CharField(max_length=20, verbose_name='标题')),
                ('post_datail', models.CharField(default=None, max_length=100, verbose_name='内容')),
                ('category', models.CharField(max_length=20, verbose_name='类别')),
                ('date', models.DateField(default=None, verbose_name='发布时间')),
                ('post_img', models.ImageField(null=True, upload_to='postimg', verbose_name='配图')),
            ],
            options={
                'verbose_name': '详情',
                'db_table': 'post',
                'verbose_name_plural': '所有详情',
            },
        ),
        migrations.CreateModel(
            name='User_PostInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('post_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='forum.PostInfo', verbose_name='详情ID')),
            ],
            options={
                'verbose_name': '联合表',
                'db_table': 'user_with_post',
                'verbose_name_plural': '所有联合表',
            },
        ),
        migrations.CreateModel(
            name='UserInfo',
            fields=[
                ('user_id', models.AutoField(max_length=11, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.CharField(max_length=20, verbose_name='用户名')),
                ('password', models.CharField(max_length=20, verbose_name='密码')),
                ('gender', models.IntegerField(choices=[(0, 'male'), (1, 'female')], default=1, verbose_name='性别')),
                ('phone', models.IntegerField(default=None, verbose_name='手机号')),
                ('email', models.CharField(default=None, max_length=20, verbose_name='邮箱')),
                ('user_img', models.ImageField(null=True, upload_to='userimg', verbose_name='头像')),
            ],
            options={
                'verbose_name': '用户',
                'db_table': 'user',
                'verbose_name_plural': '所有用户',
            },
        ),
        migrations.AddField(
            model_name='user_postinfo',
            name='user_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='forum.UserInfo', verbose_name='用户ID'),
        ),
    ]
