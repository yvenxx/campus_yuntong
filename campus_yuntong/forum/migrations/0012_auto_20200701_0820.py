# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2020-07-01 00:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0011_auto_20200630_1252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postinfo',
            name='reviewed',
            field=models.IntegerField(choices=[('unreviewed', 'unreviewed'), ('past', 'past'), ('notpast', 'notpast')], default=0, verbose_name='审核'),
        ),
    ]
