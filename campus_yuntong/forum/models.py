from django.db import models

# Create your models here.

class UserInfo(models.Model):
    GENDER_CHOICES = (
        (0, 'male'),
        (1, 'female')
    )
    user_id = models.AutoField(max_length=11, primary_key=True,verbose_name='ID')
    user = models.CharField(max_length=20, verbose_name='用户名')
    password = models.CharField(max_length=20, verbose_name='密码')
    gender = models.IntegerField(choices=GENDER_CHOICES, default=1, verbose_name='性别')
    phone = models.BigIntegerField(null=True,verbose_name='手机号')
    email = models.CharField(max_length=20, null=True, verbose_name='邮箱')
    user_img = models.ImageField(upload_to="static/img/user_img",default="https://s1.ax1x.com/2020/06/19/Nu8fQU.th.jpg",null=True,verbose_name='头像')

    class Meta:
        db_table = 'user'  # 指明数据库表名
        verbose_name = '用户'  # 在admin站点中显示的名称
        verbose_name_plural = "所有用户"  # 显示的复数名称

    def __str__(self):
        """定义每个数据对象的显示信息"""
        return self.user


class PostInfo(models.Model):
    post_id = models.AutoField(max_length=11, primary_key=True,verbose_name='POSTID')
    post_detail = models.CharField(max_length=100, default=None, verbose_name='内容')
    CATEGORY_CHOICES = (
        ('confession','confession'),
        ('lost','lost')
    )
    category = models.CharField(choices=CATEGORY_CHOICES,max_length=20, verbose_name='类别')
    date = models.DateField(auto_now=True, verbose_name='发布时间')
    post_img = models.ImageField(upload_to='static/img/post_img', null=True, verbose_name='配图')
    user_id = models.IntegerField(verbose_name='userID',default=None)
    REVIEWED_CHOICES = (
        ('unreviewed','unreviewed'),
        ('past','past'),
        ('notpast','notpast')
    )
    reviewed = models.CharField(choices=REVIEWED_CHOICES,verbose_name="审核",max_length=20,default="unreviewed")
    class Meta:
        db_table = 'post'
        verbose_name = '详情'
        verbose_name_plural = "所有详情"

    def __str__(self):
        """定义每个数据对象的显示信息"""
        return self.post_detail

