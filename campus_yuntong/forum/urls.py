from django.conf.urls import url

from forum import views

urlpatterns = [
    url(r'scenery/',views.scenery),
    url(r'lost/',views.Lost.as_view()),
    url(r'user/',views.User_center.as_view()),
    url(r'to_publish/',views.to_publish),
    url(r'post/delete/(?P<postid>[0-9]+)/', views.deletePost),
    url(r'change_pwd/',views.change_pwd),
    url(r'change_userdata/',views.ChangeUserData.as_view()),

    url(r'post/',views.create_post.as_view()),


    url(r'', views.Confession.as_view()),
]