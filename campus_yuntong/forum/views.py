from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.template import loader
from django.views import View
from rest_framework.views import APIView

from forum.models import UserInfo
from forum.models import PostInfo
from forum.utils.visit_info import day_visit


def deletePost(request,postid):
    post = PostInfo.objects.get(post_id=postid)
    post.delete()
    return redirect('/forum/user/')


def scenery(request):
    template = loader.get_template('forum/scenery.html')
    context = {}
    return HttpResponse(template.render(context,request))


class Confession(APIView):

    def get(self,request):

        if not request.COOKIES.get("is_login"):
            return redirect('/account/login')
        # 访问量
        day_visit(request)

        user = request.COOKIES.get("user_name")
        dbuser = UserInfo.objects.get(user=user)
        user_list = UserInfo.objects.all()

        confession_list = PostInfo.objects.filter(category="confession",reviewed="past")

        template = loader.get_template('forum/Confession.html')

        context = {
            "user":dbuser,
            "post_list":confession_list,
            'category': 'confession',
            'user_list':user_list,
        }
        return HttpResponse(template.render(context))


class Lost(APIView):

    def get(self, request):
        if not request.COOKIES.get("is_login"):
            return redirect('/account/login')

        user = request.COOKIES.get("user_name")
        dbuser = UserInfo.objects.get(user=user)

        confession_list = PostInfo.objects.filter(category="Lost",reviewed="past")

        template = loader.get_template('forum/Confession.html')

        context = {
            "user": dbuser,
            "post_list": confession_list,
            'category':'lost'
        }
        return HttpResponse(template.render(context))


class User_center(APIView):

    def get(self,request):

        if not request.COOKIES.get("is_login"):
            return redirect('/account/login')

        user = request.COOKIES.get("user_name")
        dbuser = UserInfo.objects.get(user=user)
        user_id = dbuser.user_id

        confession_list = PostInfo.objects.filter(user_id=user_id)

        template = loader.get_template('forum/Personal_center.html')

        context = {
            "user": dbuser,
            "user_post":confession_list
        }
        return HttpResponse(template.render(context))


def to_publish(request):
    # 判断是否的登录
    if not request.COOKIES.get("is_login"):
        return redirect('/account/login')
    # 在cookie中获取用户id
    user_id = request.COOKIES.get("user_id")
    # 链接书据库查询   固定句式 UserInfo.objects.[get(数据库字段="要查寻的字段") , filter ,all()]
    dbuser = UserInfo.objects.get(user_id=user_id)

    # 传给前端的书据
    context = {
        'user': dbuser,
    }
    # 获取模板
    template = loader.get_template('forum/Publish.html')

    return HttpResponse(template.render(context))


class create_post(View):

    def post(self,request):
        post = request.POST
        post_detail = post['desc']
        post_sex = post['sex']

        post_img = request.FILES.get('img')

        user_id = request.COOKIES.get("user_id")

        dbpost = PostInfo(post_detail=post_detail,post_img=post_img,category=post_sex,user_id=user_id)
        dbpost.save()
        # return HttpResponse(post_img)
        return redirect('/forum/user/')


class ChangeUserData(View):

    def post(self,request):
        if not request.COOKIES.get("is_login"):
            return redirect('/account/login')
        json = request.POST
        phone = json['phone']
        user_img = request.FILES.get('img')
        email = json['email']
        user_id = request.COOKIES.get("user_id")
        dbuser = UserInfo.objects.get(user_id=user_id)
        dbuser.phone = phone
        dbuser.email = email
        dbuser.user_img = user_img
        dbuser.save()
        return redirect('/forum/user/')



def change_pwd(request):
    json = request.POST
    password = json['oldpwd']
    newpwd = json['newpwd']
    userid = request.COOKIES.get("user_id")

    dbuser = UserInfo.objects.get(user_id=userid)
    if password == dbuser.password:
        dbuser.password = newpwd
        dbuser.save()
        # return render(request, '../../forum/templates/forum/Personal_center.html', {'message': "修改成功"})

        return redirect('/account/logout/')
    else:
        return redirect('/forum/user/')
        # return render(request, '../../forum/templates/forum/Personal_center.html', {'message': "修改失败"})


