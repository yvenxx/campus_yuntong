

//账号
var a = 0;
var b = 0;
var c = 0;

function user(){
	var user=$(".user").val();
	//手机号
	var reg1 = /^1[3-9]\d{9}$/;
	var res1 = new RegExp(reg1);
	//邮箱
	var reg2 = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+\.[a-zA-Z0-9_-]+/;
	var res2 = new RegExp(reg2);
	//用户名
	var reg3 = /^[A-Za-z]+\w{1,7}$/;
	var res3 = new RegExp(reg3);


	if(!user){
		$(".ut").text("账号不能为空");
        $(".ut").fadeIn(1000);
        $(".ut").fadeOut(1000);
	}else{
		if((res1.test(user)||res2.test(user)||res3.test(user))==false){

			$(".ut").text("输入手机号/邮箱/用户名有误");
		    $(".ut").fadeIn(1000);//显示
		    $(".ut").fadeOut(1000);//隐藏
		}else{
			$(".ut").hide()
		}

	}
}

//密码
function password(){
	var password=$(".password").val();

	if(!password){
		$(".mt").text("密码不能为空");
        $(".mt").fadeIn(1000);//显示
        $(".mt").fadeOut(1000);//隐藏
	}else{
		$(".mt").hide();
	}
}
$(".user").blur(function(){    //blur:失去焦点(输入完成)
			user();
});
$(".password").blur(function(){    //blur:失去焦点(输入完成)
			password();

});
$(".login").click(function() {
	user();
	password();
});
