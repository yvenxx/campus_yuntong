var a=true;
var b=true;
var c=true;
// 验证账号
function user(){
    var re =  /^[a-zA-Z]+\w{1,10}$/;  //判断字符串是否为数字和字母组合
    var username=$(".user").val();

    if(!username){
        $(".ut").text("账号不能为空");
        $(".ut").fadeIn(1000);//显示
        $(".ut").fadeOut(2000);//隐藏
        // return false;//账号为空就不能继续往下写
        a=false;
    }else{
        if(!re.test(username)){
            $(".ut").text("账号规则不匹配! 开头必须为字母，4-11位字符!");
            $(".ut").fadeIn(1000);//显示
            $(".ut").fadeOut(2000);//隐藏
            a=false;
        }else{
            a=true;
        }
    }
}

// 验证密码
function pass(){
    var passwordone=$(".passwordone").val();
    var passwordtwo=$(".passwordtwo").val();
    

    if (!passwordone) {
        $(".mta").text("密码不能为空！");
        $(".mta").fadeIn(1000);//显示
        $(".mta").fadeOut(2000);//隐藏
        b=false;
    }else{
        if(passwordone.length>16||passwordone.length<8){
            $(".mta").text("密码长度8-16位");
            $(".mta").fadeIn(1000);//显示
            $(".mta").fadeOut(2000);//隐藏
            b=false;
        }else{
            b=true;
        }
    }
}
// 验证两次密码是否一致
function repass(){
    var passwordone=$(".passwordone").val();
    var passwordtwo=$(".passwordtwo").val();

    if (passwordone!=passwordtwo) {
        $(".mtb").text("两次密码不同！");
        $(".mtb").fadeIn(1000);//显示
        $(".mtb").fadeOut(2000);//隐藏
        c=false;
    }else{
        c=true;
    }
}

$(".user").blur(function(){
    user();
})
$(".passwordone").blur(function(){
    pass();
})
$(".passwordtwo").blur(function(){
    repass();
})
