from django.conf.urls import url

from . import views

urlpatterns = [
    url('^image_codes/(?P<image_code_id>[\w-]+)/$',views.ImageCodeView.as_view()),
    url('img_verify/',views.Img_verify.as_view()),
    url('user_verify',views.User_verify.as_view()),
    url('register_verify/',views.Register_verify.as_view()),
    url('back_login_verify/', views.BackLogin_verify.as_view()),
    url('login_verify/',views.Login_verify.as_view()),
]