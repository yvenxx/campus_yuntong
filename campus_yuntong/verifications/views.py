from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

from django.views import View
from django_redis import get_redis_connection
from backstage.models import BackUser
from forum.models import UserInfo
from . import constants
from .utils.captcha.captcha.captcha.captcha import captcha


class ImageCodeView(View):
    """
    图片验证码
    """
    def get(self, request, image_code_id):
        """
        获取图片验证码
        """
        # 生成验证码图片
        text, image = captcha.generate_captcha()
        redis_conn = get_redis_connection("verify_codes")
        redis_conn.setex("img_%s" % image_code_id, constants.IMAGE_CODE_REDIS_EXPIRES, text)
        # 固定返回验证码图片数据，不需要REST framework框架的Response帮助我们决定返回响应数据的格式
        # 所以此处直接使用Django原生的HttpResponse即可
        return HttpResponse(image, content_type="images/jpg")


class Img_verify(View):

    def get(self, request):
        json = request.GET
        redis_conn = get_redis_connection("verify_codes")
        redis_imgcode = redis_conn.get('img_' + json['img_code'])

        if str(redis_imgcode.decode().lower().strip()) == str(json['img_input'].lower().strip()):
            return JsonResponse({
                'json': True
            })
        else:
            return JsonResponse({
                'json': False
            })


class User_verify(View):

    def get(self,request):
        user = request.GET['user']
        dbuser = "None"
        try:
            dbuser = UserInfo.objects.get(user=user)
        except:
            dbuser = None
        if dbuser == None:
            return JsonResponse({
                'json': False
            })
        else:
            return JsonResponse({
                'json': True
            })


class Register_verify(View):

    def post(self,request):
        json = request.POST
        user = json['username']
        password = json['pwdone']
        dbuser = UserInfo(user=user,password=password)
        dbuser.save()
        return redirect('/account/login')


class Login_verify(View):

    def post(self,request):
        if request.COOKIES.get("is_login"):
            return redirect('/forum/')
        json = request.POST
        user = json['username']
        password = json['password']
        dbuser = ""
        message = "账户或密码错误"
        try:
            dbuser = UserInfo.objects.get(user=user)
        except:
            dbuser = None
        if dbuser == None:
            message="账户或密码错误"
            return render(request, '../../account/templates/account/login.html', {'message': message})
        else:
            if dbuser.password == password:
                rep = redirect('/forum/')
                rep.set_cookie("is_login", True)
                username = dbuser.user
                userid = dbuser.user_id
                userimg = dbuser.user_img
                rep.set_cookie("user_name",username)
                rep.set_cookie("user_id",userid)
                rep.set_cookie("user_img",userimg)
                request.session["user"] = username
                return rep
            else:
                return render(request, '../../account/templates/account/login.html', {'message': message})


class BackLogin_verify(View):

    def post(self,request):
        if request.COOKIES.get("is_login_back"):
            return redirect('/backstage/backstageManage/')
        json = request.POST
        user = json['username']
        password = json['password']
        dbuser = ""
        message = "账户或密码错误"
        try:
            dbuser = BackUser.objects.get(back_user=user)
        except:
            dbuser = None
        if dbuser == None:
            message="账户或密码错误"
            return render(request,'../../backstage/templates/backstage/back_login.html',{'message': message})
        else:
            if dbuser.back_password == password:
                rep = redirect('/backstage/backstageManage/')
                username = dbuser.back_user
                rep.set_cookie("is_login_back", True)
                rep.set_cookie("back_user",username)
                return rep
            else:
                return render(request, '../../backstage/templates/backstage/back_login.html', {'message': message})